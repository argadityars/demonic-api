<?php
if (getenv('ENV') === 'dev') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
require 'start.php';
