<?php namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = ['username', 'password'];
    protected $hidden = ['password'];

    public function ttcs()
    {
        return $this->belongsToMany('App\Models\TTC', 'user_ttc', 'user_id', 'ttc_id');
    }
}
