<?php namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class TTC extends Model
{
    protected $table = 'ttcs';
    protected $fillable = ['name', 'location_id', 'chat_id', 'group'];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_ttc', 'ttc_id', 'user_id');
    }

    public function floors()
    {
        return $this->hasMany('App\Models\Floor', 'ttc_id');
    }
}
