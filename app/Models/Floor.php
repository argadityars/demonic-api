<?php namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $table = 'floors';
    protected $fillable = ['ttc_id', 'name', 'map'];

    public function ttc()
    {
        return $this->belongsTo('App\Models\TTC');
    }

    public function rooms()
    {
        return $this->hasMany('App\Models\Room');
    }
}
