<?php namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';
    protected $fillable = ['room_id', 'name', 'token'];

    public function room()
    {
        return $this->belongsTo('App\Models\Room');
    }

    public function sensor()
    {
        return $this->hasOne('App\Models\Sensor');
    }
}
