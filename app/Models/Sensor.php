<?php namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    protected $table = 'sensors';
    protected $fillable = ['device_id', 'ip', 'temperature', 'humidity', 'csq', 'lat', 'lng', 'smoke', 'light', 'rain', 'ex1', 'ex2', 'ex3', 'ex4'];

    public function device()
    {
        return $this->belongsTo('App\Models\Device');
    }
}
