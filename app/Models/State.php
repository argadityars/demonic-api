<?php namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
    protected $fillable = ['device_id', 'column', 'state'];

    public function device()
    {
        return $this->belongsTo('App\Models\Device');
    }
}
