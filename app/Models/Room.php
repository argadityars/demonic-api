<?php namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';
    protected $fillable = ['floor_id', 'name'];

    public function floor()
    {
        return $this->belongsTo('App\Models\Floor');
    }

    public function devices()
    {
        return $this->hasMany('App\Models\Device');
    }
}
