<?php namespace Routes;

use App\Router;

Router::csrfVerifier(new \App\Middlewares\CsrfVerifier());
Router::group([
    'prefix' => PREFIX_URL . '/api/v3',
    'middleware' => \App\Middlewares\ApiVerification::class,
    'exceptionHandler' => \App\Handlers\APIExceptionHandler::class],
    function () {
        Router::post('/auth/register', 'App\Controllers\AuthController@register');
        Router::post('/auth/login', 'App\Controllers\AuthController@login');
        Router::post('/home', 'App\Controllers\HomeController@home');
        Router::resource('/user', 'App\Controllers\UserController');
        Router::get('/user/{user}/ttc/{ttc}', 'App\Controllers\UserController@ttc');
        Router::resource('/ttc', 'App\Controllers\TTCController');
        Router::resource('/floor', 'App\Controllers\FloorController');
        Router::resource('/room', 'App\Controllers\RoomController');
        Router::resource('/device', 'App\Controllers\DeviceController');
        Router::resource('/sensor', 'App\Controllers\SensorController');

        Router::get('/state/{device}/column/{column}', 'App\Controllers\StateController@show');
        Router::post('/state', 'App\Controllers\StateController@update');

        Router::get('/sms', 'App\Controllers\MessageController@send');
    }
);
