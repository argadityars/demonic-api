<?php namespace Routes;

use App\Router;

Router::csrfVerifier(new \App\Middlewares\CsrfVerifier());
Router::group([
    'prefix' => PREFIX_URL],
    function () {
        Router::get('/', 'App\Controllers\HomeController@index')->setName('home');
    }
);
