<?php namespace App\Handlers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Pecee\Http\Request;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;
use Pecee\SimpleRouter\Handlers\IExceptionHandler;

class APIExceptionHandler implements IExceptionHandler
{
    /**
     * @param Request $request
     * @param \Exception $error
     * @throws \Exception
     */
    public function handleError(Request $request, \Exception $error): void
    {
        if ($error instanceof NotFoundHttpException) {
            http_response_code(404);
            response()->json([
                'code' => $error->getCode(),
                'message' => $error->getMessage(),
            ]);
            return;
        } else if ($error instanceof ModelNotFoundException) {
            http_response_code(404);
            response()->json([
                'code' => 404,
                'message' => $error->getMessage(),
            ]);
            return;
        }
        throw $error;
    }
}
