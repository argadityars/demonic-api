<?php namespace App\Controllers;

use App\Models\Floor;

class FloorController
{
    public function index()
    {
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => Floor::all(),
        ]);
    }

    public function store()
    {
        $image = input()->file('map');
        $filename = upload($image, storage('/map'));
        if ($filename) {
            $input = input()->all();
            $input['map'] = asset('/map/') . $filename;
            $floor = Floor::create($input);

            http_response_code(201);
            return response()->json([
                'code' => 201,
                'message' => 'Data stored successfully',
                'data' => $floor,
            ]);
        }
    }

    public function show($id)
    {
        $floor = Floor::with('rooms')->findOrFail([$id]);
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => $floor,
        ]);
    }
}
