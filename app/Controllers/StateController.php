<?php namespace App\Controllers;

use App\Models\State;

class StateController
{
    public function show($device, $column)
    {
        $state = State::where('device_id', $device)->where('column', $column)->first();

        http_response_code(200);
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => $state,
        ]);
    }

    public function update()
    {
        $device_id = input()->post('device_id');
        $column = input()->post('column');
        $state = input()->post('state');

        $update = State::where('device_id', $device_id)
            ->where('column', $column)
            ->update(['state' => $state]);

        http_response_code(200);
        return response()->json([
            'code' => 200,
            'message' => 'Data updated successfully',
            'data' => State::findOrFail($update),
        ]);
    }
}
