<?php namespace App\Controllers;

use App\Models\User;

class AuthController
{
    public function login()
    {
        $username = input()->post('username');
        $password = input()->post('password');

        $user = User::where('username', $username)->first();
        if (password_verify($password, $user->password)) {
            $this->setToken($user);

            return response()->json([
                'code' => 200,
                'message' => 'Data retrieved successfully',
                'data' => $user,
            ]);
        } else {
            http_response_code(401);
            return response()->json([
                'code' => 401,
                'message' => 'Unable to login. Please check your username/password',
            ]);
        }
    }

    public function register()
    {
        try {
            $user = User::create([
                'username' => input()->post('username')->value,
                'password' => password_hash(input()->post('password'), PASSWORD_DEFAULT),
            ]);

            http_response_code(201);
            return response()->json([
                'code' => 201,
                'message' => 'Data stored successfully',
                'data' => $user,
            ]);
        } catch (\PDOException $e) {
            http_response_code(400);
            return response()->json([
                'code' => 400,
                'message' => 'Unable to store data.',
                'trace' => $e->getMessage(),
            ]);
        }
    }

    public function setToken($user)
    {
        $user->token = md5($user->username);
        $user->save();
    }
}
