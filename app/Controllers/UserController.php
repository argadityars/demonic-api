<?php namespace App\Controllers;

use App\Models\User;

class UserController
{
    public function index()
    {
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => User::all(),
        ]);
    }

    public function store()
    {
        http_response_code(400);
        return response()->json([
            'code' => 400,
            'message' => 'Unable to register user',
            'trace' => 'This function is deprecated, use auth/register instead',
        ]);
    }

    public function show($id)
    {
        $user = User::with('ttcs')->findOrFail([$id]);

        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => $user,
        ]);
    }

    public function ttc($userId, $ttcId)
    {
        try {
            $user = User::findOrFail($userId);
            $user->ttcs()->attach($ttcId);

            return response()->json([
                'code' => 200,
                'message' => 'User attached to TTC successfully',
                'data' => User::with('ttcs')->findOrFail([$userId]),
            ]);
        } catch (\PDOException $e) {
            http_response_code(400);
            return response()->json([
                'code' => 400,
                'message' => 'Unable to attach user to TTC',
                'trace' => $e->getMessage(),
            ]);
        }
    }
}
