<?php namespace App\Controllers;

use App\Models\TTC;

class TTCController
{
    public function index()
    {
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => TTC::all(),
        ]);
    }

    public function store()
    {
        $ttc = TTC::create(input()->all());

        http_response_code(201);
        return response()->json([
            'code' => 201,
            'message' => 'Data stored successfully',
            'data' => $ttc,
        ]);
    }

    public function show($id)
    {
        $ttc = TTC::with('floors')->findOrFail([$id]);
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => $ttc,
        ]);
    }
}
