<?php namespace App\Controllers;

use App\Models\Device;

class DeviceController
{
    public function index()
    {
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => Device::all(),
        ]);
    }

    public function store()
    {
        try {
            $device = Device::create(input()->all());

            http_response_code(201);
            return response()->json([
                'code' => 201,
                'message' => 'Data stored successfully',
                'data' => $device,
            ]);
        } catch (\PDOException $e) {
            http_response_code(400);
            return response()->json([
                'code' => 400,
                'message' => 'Unable to store data.',
                'trace' => $e->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        $device = Device::with(['sensor' => function ($q) {
            $q->latest('sensors.created_at');
        }, 'room.floor.ttc'])->findOrFail($id);

        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => $device,
        ]);
    }
}
