<?php namespace App\Controllers;

use App\Models\Room;
use App\Models\TTC;

class HomeController
{
    public static function index()
    {
        return 'This is a restricted area. Please stay away!';
    }

    public function home()
    {
        $user_id = input()->post('user_id');
        $ttcs = TTC::select(['id', 'name'])->whereHas('users', function ($q) use ($user_id) {
            $q->where('users.id', $user_id);
        })->get();

        foreach ($ttcs as $ttc) {
            if (count($ttc->floors) > 0) {
                $rooms = Room::leftJoin('floors', 'rooms.floor_id', '=', 'floors.id')
                    ->leftJoin('devices', 'devices.room_id', '=', 'rooms.id')
                    ->whereNotNull('devices.id')
                    ->select(['rooms.id', 'rooms.name AS room', 'floors.name AS floor', 'devices.id AS device_id'])
                    ->get();

                $ttc['data'] = $rooms;
            } else {
                $ttc['data'] = [];
            }
            unset($ttc['floors']);
        }

        return json_encode(array(
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => $ttcs,
        ));
    }
}
