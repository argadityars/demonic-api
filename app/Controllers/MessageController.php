<?php namespace App\Controllers;

use App\Models\Sensor;

class MessageController
{
    public function httpRequest($url, $header = array(), $method = 'GET', $data = array(), $mask = null)
    {
        $timeout = 60;
        $dataString = '';

        if (($mask & CONTENT_TYPE_JSON) == CONTENT_TYPE_JSON) {
            $dataString = json_encode($data);
        } else if (($mask & CONTENT_TYPE_FORM) == CONTENT_TYPE_FORM) {
            foreach ($data as $key => $value) {
                $dataString .= (($dataString ? '&' : '') . $key . '=' . $value);
            }
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if ($method = 'POST') {
            curl_setopt($ch, CURLOPT_POST, count($data));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function generateNonce()
    {
        return mt_rand();
    }

    public function getCreatedTime()
    {
        date_default_timezone_set("UTC");
        $date = new \DateTime();
        return $date->format('Y-m-d\TH:i:s\Z');
    }

    public function generatePasswordDigest($nonce, $createdTime, $appSecret)
    {
        $concat = $nonce . $createdTime . $appSecret;
        return base64_encode(hash('sha256', $concat, true));
    }

    public function getXWSSE($appKey, $nonce, $createdTime, $passwordDigest)
    {
        $xwsse = "UsernameToken Username=\"" . $appKey . "\", PasswordDigest=" .
            "\"" .
            $passwordDigest .
            "\", " .
            "Nonce=" .
            "\"" .
            $nonce .
            "\", " .
            "Created=" .
            "\"" .
            $createdTime .
            "\"";

        return $xwsse;
    }

    public function send()
    {
        $sensor = Sensor::where('device_id', 1)
            ->whereDate('created_at', \Carbon\Carbon::today())
            ->selectRaw('format(avg(temperature), 2) as temp, format(avg(humidity), 2) as hum, format(avg(smoke), 2) as smoke, format(avg(light), 2) as light')
            ->first();

        $appKey = 'e4f7757fd5b94e049a45fc05df660f28';
        $appSecret = '5bab92806f77d851';

        $nonce = $this->generateNonce();
        $createdTime = $this->getCreatedTime();
        $passwordDigest = $this->generatePasswordDigest($nonce, $createdTime, $appSecret);
        $xwsse = $this->getXWSSE($appKey, $nonce, $createdTime, $passwordDigest);

        $header = array(
            'Authorization: WSSE realm="SDP", profile="UsernameToken", type="AppKey"',
            'X-WSSE: ' . $xwsse,
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
        );

        $data = array(
            'from' => 'FREENUM0',
            'to' => '6282221803616',
            'body' => "Daily report Demon:\nTemp: $sensor->temp,\nHum: $sensor->hum,\nDangerous Gas: $sensor->smoke,\nLight: $sensor->light",
        );

        $url = 'https://api.telkomsel.com/sms/sendSms/v1';
        $result = $this->httpRequest($url, $header, 'POST', $data, CONTENT_TYPE_FORM);

        echo $result;
    }
}
