<?php namespace App\Controllers;

use App\Models\Room;

class RoomController
{
    public function index()
    {
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => Room::all(),
        ]);
    }

    public function store()
    {
        $room = Room::create(input()->all());

        http_response_code(201);
        return response()->json([
            'code' => 201,
            'message' => 'Data stored successfully',
            'data' => $room,
        ]);
    }

    public function show($id)
    {
        $room = Room::findOrFail($id);
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => $room,
        ]);
    }
}
