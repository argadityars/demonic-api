<?php namespace App\Controllers;

use App\Models\Device;
use App\Models\Sensor;
use Curl\Curl;

class SensorController
{
    public function index()
    {
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => Sensor::all(),
        ]);
    }

    public function store()
    {
        $data = array(
            'device_id' => input()->post('device_id')->value,
            'ip' => input()->post('ip')->value,
            'temperature' => input()->post('temperature')->value,
            'humidity' => input()->post('humidity')->value,
            'lat' => input()->post('lat')->value,
            'lng' => input()->post('lng')->value,
            'smoke' => input()->post('smoke')->value,
            'light' => input()->post('light')->value,
            'rain' => input()->post('rain')->value,
        );
        if ($this->sendToConnectinc($data)) {
            $sensor = Sensor::create($data);

            http_response_code(201);
            return response()->json([
                'code' => 201,
                'message' => 'Data stored successfully',
                'data' => $sensor,
            ]);
        } else {
            http_response_code(400);
            return response()->json([
                'code' => 400,
                'message' => 'Unable to store data',
            ]);
        }
    }

    public function show($id)
    {
        $sensor = Sensor::findOrFail($id);
        return response()->json([
            'code' => 200,
            'message' => 'Data retrieved successfully',
            'data' => $sensor,
        ]);
    }

    public function sendToConnectinc($data)
    {
        $data['location'] = $data['lat'] . ',' . $data['lng'];
        unset($data['lat']);
        unset($data['lng']);

        $device = Device::findOrFail($data['device_id']);

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->post(CONNECTINC_PUSH_URL, json_encode(array(
            "token_key" => $device->token,
            "json_data" => json_encode($data),
        )));

        return $curl->error ? false : true;
    }
}
