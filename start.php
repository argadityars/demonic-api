<?php
require 'vendor/autoload.php';

// Set up Dotenv
$dotenv = \Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

require 'config.php';

use App\Models\Database;
use App\Router;

// Set up Database
new Database();

// Set up Router
Router::start();
