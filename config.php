<?php

date_default_timezone_set("Asia/Jakarta");
defined("DBDRIVER") or define("DBDRIVER", "mysql");
defined("DBHOST") or define("DBHOST", getenv('DBHOST'));
defined("DBNAME") or define("DBNAME", getenv('DBNAME'));
defined("DBUSER") or define("DBUSER", getenv('DBUSER'));
defined("DBPASS") or define("DBPASS", getenv('DBPASS'));

defined("BASE_URL") or define("BASE_URL", getenv('BASE_URL'));
defined("PREFIX_URL") or define("PREFIX_URL", getenv('PREFIX_URL'));
defined("BASE_PATH") or define("BASE_PATH", dirname(__FILE__));
defined("STORAGE_PATH") or define("STORAGE_PATH", BASE_PATH . '/storage');
defined("ASSET_PATH") or define("ASSET_PATH", '/storage');
defined("CONNECTINC_PUSH_URL") or define("CONNECTINC_PUSH_URL", 'https://connectinc.telkomsel.com/tsel-iot/api/thing/push-data');

defined("CONTENT_TYPE_FORM") or define('CONTENT_TYPE_FORM', 0x0001);
defined("CONTENT_TYPE_JSON") or define('CONTENT_TYPE_JSON', 0x0002);
